package com.trainee.photoapp.adapters

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.trainee.photoapp.data.NetworkPhoto
import com.trainee.photoapp.databinding.GridViewItemBinding

class PhotoGridAdapter(private val onClickListener: OnClickListener ) : ListAdapter<NetworkPhoto,
        PhotoGridAdapter.MarsPhotoViewHolder>(DiffCallback) {

    class MarsPhotoViewHolder(
        private var binding:
        GridViewItemBinding
    ) :
        RecyclerView.ViewHolder(binding.root) {
        fun bind(MarsPhoto: NetworkPhoto) {
            binding.photo = MarsPhoto
            binding.executePendingBindings()
        }

    }

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): MarsPhotoViewHolder {
        return MarsPhotoViewHolder(
            GridViewItemBinding.inflate(
                LayoutInflater.from(parent.context)
            )
        )
    }

    override fun onBindViewHolder(holder: MarsPhotoViewHolder, position: Int) {
        val networkPhoto = getItem(position)
        holder.itemView.setOnClickListener {
            onClickListener.onClick(networkPhoto)
        }
        holder.bind(networkPhoto)
    }
    companion object DiffCallback : DiffUtil.ItemCallback<NetworkPhoto>() {
        override fun areItemsTheSame(oldItem: NetworkPhoto, newItem: NetworkPhoto): Boolean {
            return oldItem.id == newItem.id
        }

        override fun areContentsTheSame(oldItem: NetworkPhoto, newItem: NetworkPhoto): Boolean {
            return oldItem.url == newItem.url
        }
    }

    class OnClickListener(val clickListener: (networkPhoto:NetworkPhoto) -> Unit) {
        fun onClick(networkPhoto:NetworkPhoto) = clickListener(networkPhoto)
    }
}