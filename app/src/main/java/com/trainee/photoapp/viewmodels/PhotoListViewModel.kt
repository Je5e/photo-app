package com.trainee.photoapp.viewmodels

import android.app.Application
import androidx.lifecycle.*
import com.trainee.photoapp.data.NetworkPhoto
import com.trainee.photoapp.data.PhotoApiStatus
import com.trainee.photoapp.data.PhotoDatabase
import com.trainee.photoapp.data.PhotoRepository
import kotlinx.coroutines.launch

class PhotoListViewModel(application: Application) : ViewModel() {

    private val photoRepository = PhotoRepository(PhotoDatabase.getDatabase(application))

    private val _status = MutableLiveData<PhotoApiStatus>()

    val status: LiveData<PhotoApiStatus> = _status

    var photos = photoRepository.photos

    private val _navigateToSelectedPhoto= MutableLiveData<NetworkPhoto>()
    val navigateToSelectedPhoto: LiveData<NetworkPhoto>
        get() = _navigateToSelectedPhoto
    /**
     * Call getPhotos() on init so we can display status immediately.
     */
    init {
        getPhotosFromRepository()
    }

    private fun getPhotosFromRepository() {
        viewModelScope.launch {
            _status.value = PhotoApiStatus.LOADING
            try {
                //_photos.value = PhotoService.retrofitService.getPhotos()
                    photoRepository.refreshPhotos()
                _status.value = PhotoApiStatus.DONE
            } catch (e: Exception) {
                _status.value = PhotoApiStatus.ERROR

            }
        }
    }
    fun displayPhotoDetails(marsProperty: NetworkPhoto) {
        _navigateToSelectedPhoto.value = marsProperty
    }
    fun displayPhotoDetailsComplete() {
        _navigateToSelectedPhoto.value = null
    }
    class Factory(val app: Application) : ViewModelProvider.Factory {
        override fun <T : ViewModel?> create(modelClass: Class<T>): T {
            if (modelClass.isAssignableFrom(PhotoListViewModel::class.java)) {
                @Suppress("UNCHECKED_CAST")
                return PhotoListViewModel(app) as T
            }
            throw IllegalArgumentException("Unable to construct viewmodel")
        }
    }
}