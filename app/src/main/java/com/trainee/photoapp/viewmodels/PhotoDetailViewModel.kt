package com.trainee.photoapp.viewmodels

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.trainee.photoapp.data.NetworkPhoto

class PhotoDetailViewModel(
    marsProperty: NetworkPhoto,
    app: Application
) : AndroidViewModel(app) {

    private val _selectedProperty = MutableLiveData<NetworkPhoto>()
    val selectedProperty: LiveData<NetworkPhoto>
        get() = _selectedProperty

    init {
        _selectedProperty.value = marsProperty
    }
}