package com.trainee.photoapp.api

import com.trainee.photoapp.data.NetworkPhoto
import retrofit2.http.GET

interface PhotoApiService {
    @GET("photos")
   suspend fun getPhotos(): List<NetworkPhoto>
}