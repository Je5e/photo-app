package com.trainee.photoapp.ui

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import com.trainee.photoapp.adapters.PhotoGridAdapter
import com.trainee.photoapp.databinding.FragmentPhotoListBinding
import com.trainee.photoapp.viewmodels.PhotoListViewModel

class PhotoListFragment : Fragment() {


    private val viewModel: PhotoListViewModel by lazy {
        val activity = requireNotNull(this.activity) {
            "You can only access the viewModel after onActivityCreated()"
        }
        ViewModelProvider(this, PhotoListViewModel.Factory(activity.application))
            .get(PhotoListViewModel::class.java)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {

        val binding = FragmentPhotoListBinding.inflate(inflater)
        binding.lifecycleOwner = this

        binding.viewModel = viewModel
        binding.photosGrid.adapter = PhotoGridAdapter(PhotoGridAdapter.OnClickListener {
            viewModel.displayPhotoDetails(it)
        })
        viewModel.navigateToSelectedPhoto.observe(viewLifecycleOwner, {
            if (null != it) {
                this.findNavController().navigate(
                    PhotoListFragmentDirections.actionPhotoListFragmentToPhotoDetailFragment(it)
                )
                viewModel.displayPhotoDetailsComplete()
            }
        })
        return binding.root
    }
}