package com.trainee.photoapp.ui

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import com.trainee.photoapp.databinding.FragmentPhotoDetailBinding
import com.trainee.photoapp.viewmodels.DetailViewModelFactory
import com.trainee.photoapp.viewmodels.PhotoDetailViewModel


class PhotoDetailFragment : Fragment() {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View {

        val application = requireNotNull(activity).application
        val binding = FragmentPhotoDetailBinding.inflate(inflater)
        binding.lifecycleOwner = this

        val networkPhoto = PhotoDetailFragmentArgs.fromBundle(requireArguments()).selectedProperty
        val viewModelFactory = DetailViewModelFactory(networkPhoto, application)
        binding.viewModel = ViewModelProvider(
            this, viewModelFactory).get(PhotoDetailViewModel::class.java)

        return binding.root
    }
}