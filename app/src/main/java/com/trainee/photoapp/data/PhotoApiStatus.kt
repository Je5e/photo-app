package com.trainee.photoapp.data

enum class PhotoApiStatus {
    LOADING, ERROR, DONE
}