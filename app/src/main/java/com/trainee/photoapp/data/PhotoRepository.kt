package com.trainee.photoapp.data

import androidx.lifecycle.LiveData
import androidx.lifecycle.Transformations
import com.trainee.photoapp.api.PhotoService
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

class PhotoRepository(private val photoDatabase:PhotoDatabase) {

    val photos: LiveData<List<NetworkPhoto>> = Transformations.map( photoDatabase.photoDao.getPhotos()){
        it.asDomainModel()
    }


    suspend fun refreshPhotos() {
        withContext(Dispatchers.IO) {
            val photos = PhotoService.retrofitService.getPhotos()
            photoDatabase.photoDao.insertAll(photos.asDatabaseModel())
        }
    }

}