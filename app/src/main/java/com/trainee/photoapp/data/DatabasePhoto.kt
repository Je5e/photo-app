package com.trainee.photoapp.data

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
data class DatabasePhoto constructor(
    @PrimaryKey
    val id: Int,
    val albumId: Int,
    val title: String,
    val url: String,
    val thumbnailUrl: String
)

fun List<DatabasePhoto>.asDomainModel(): List<NetworkPhoto> {
    return map {
        NetworkPhoto(
            url = it.url,
            title = it.title,
            id = it.id,
            albumId = it.albumId,
            thumbnailUrl = it.thumbnailUrl)
    }
}
